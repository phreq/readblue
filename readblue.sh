#!/bin/zsh

#### Stampa delle statistiche ####
stats() {
	awk -F "," '{sum=sum+$3; median=sum/NR} END{print "\nMedian: " median}' $FILE
	awk -F "," '{max = (NR==1 || $3>max ? $3 : max); min = (NR==1 || $3<min ? $3 : min); delta=max-min} END{print "Worst: " min "\nBest:" max "\nDelta: " delta}' $FILE
	echo $ANTENNA >> $FILE
	echo "[INFO] Antenna appended to file."
	echo "___________________"
	exit $?
}

trap stats SIGINT

#### Controllo il numero degli argomenti ####
if [ "$#" -lt 2 ]
then
	echo "[ERROR] Check your arguments"
	exit $?
fi

echo "BlueUp listener 0.1"

if [ "$#" -eq 3 ] 
then
    ANTENNA=$3
    echo "[INFO] Listeing antenna named: $ANTENNA"
fi

#### Info sul file ####
FILE=$2
if [[ -f $FILE ]]
then
	echo "[INFO] file named "$FILE" already exists, appending to it"
else
	echo "[INFO] file named "$FILE" doesn't exists, creating it"
fi 

#### Scrittura dei valori su file ####
while true
do
	response=$(curl -s -X GET "http://$1/api/beacons/blueup" -H  "accept: application/json")

	echo $response | jq -r '.beacons | map([.timestamp, .serial, .safety.rssi] | join(", ")) | join("\n")' | tee -a $FILE
	sleep 4
done
